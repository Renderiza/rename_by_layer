=begin
(c) Bagatelo 2013
(c) Renderiza 2013

Permission to use, copy, modify, and distribute this software for
any purpose and without fee is hereby granted, provided the above
copyright notice appear in all copies.

THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

== Information
Authors:: | Bagatelo | Renderiza |
Name:: Rename by Layer
Version:: 1.0.3
SU Version:: v8
Date:: 9/17/2013
Description:: Rename groups & components under same layer with layer name or name specified by user.
=end

module RND_Extensions
	#
	module RND_Renamer
		require 'sketchup.rb'
		require 'extensions.rb'
		#
		unless file_loaded?( __FILE__ )
			file_loaded( __FILE__ )
			# Create the extension.
			ext = SketchupExtension.new 'Rename by Layer', 'RND_Renamer/RND_Renamer_menus.rb'
			#
			# Attach some nice info.
			ext.creator     = '| Bagatelo | Renderiza |'
			ext.version     = '1.0.3'
			ext.copyright   = '2013, Renderiza Studio.'
			ext.description = 'Rename groups & components under same layer with layer name or name specified by user.'
			#
			# Register and load the extension on startup.
			Sketchup.register_extension ext, true
		end
		#
	end
	#
end
#
file_loaded( __FILE__ )
