=begin
(c) Bagatelo 2013
(c) Renderiza 2013

Permission to use, copy, modify, and distribute this software for
any purpose and without fee is hereby granted, provided the above
copyright notice appear in all copies.

THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

== Information
Authors:: | Bagatelo | Renderiza |
Name:: Rename by Layer
Version:: 1.0.3
SU Version:: v8
Date:: 9/17/2013
Description:: Rename groups & components under same layer with layer name or name specified by user.
=end

module RND_Extensions

	module RND_Renamer

		class Renamer

			@@model = Sketchup.active_model
			@@path = File.dirname(__FILE__)
			@@rnd_renamer = "rnd_renamer.html"
			@@renamer_files = File.join(@@path, '/RenamerFiles/')
			@@renamer_dlg = nil

			def initialize()

				@renamer_1_file =  File.join(@@path, @@rnd_renamer)

				if (!@renamer_1_file) then
					UI.messagebox("Cannot find file '#{@@rnd_renamer} in folder '#{@@path}'")
					return
				end

				if @@renamer_dlg == nil
					@@renamer_dlg = UI::WebDialog.new("Rename by Layer v.1.0.3", false, "Rename by Layer v.1.0.3", 225, 550, 70, 95, true)
					@@renamer_dlg.add_action_callback("push_frame") do |d,p|
					push_frame(d,p)
					end
				end

				@@renamer_dlg.set_size(225, 550)
				@@renamer_dlg.set_file(@renamer_1_file)
				@@renamer_dlg.show()
				
			end #def



			def push_frame(dialog,data)
				@@renamer_dlg.execute_script("document.body.style.zoom = 0.90")
				params = query_to_hash(data)

				if params['all_or'].to_s  == "all"
					@all_or = 1
				end
				if params['all_or'].to_s  == "visible"
					@all_or = 2
				end
				if params['all_or'].to_s  == "hidden"
					@all_or = 3
				end
				if params['all_or'].to_s  == "active"
					@all_or = 4
				end
				if params['all_or'].to_s  == "selection"
					@all_or = 5
				end
				###
				if params['keep'].to_s  == "true"
					@keep = true
				else
					@keep = false
				end
				###
				if params['usename'].to_s  == "ln"
					@usename = 1
				else
					@usename = 0
				end
				###
				@text_value = params['getsearch'].to_s
				###
				if params['addnum'].to_s  == "true"
					@addnum = 1
				else
					@addnum = 0
				end
				###
				if params['presu'].to_s  == "prefix"
					@presu = 1
				else
					@presu = 0
				end
				###
				@what = params['what'].to_s
				###
				@entitytype = params['entitytype'].to_s
		
				#Click Clear
				if params['empty'].to_s == "1"

					model = Sketchup.active_model
					ents = model.active_entities   
					groups = ents.grep(Sketchup::Group)
					components = ents.grep(Sketchup::ComponentInstance)
					layer = model.active_layer
					sel = model.selection
						
						if @entitytype != "comp"
							if @what != "defname"
								groups.each do |group|
									if @all_or == 1
										group.name = nil.to_s
									end
									if group.layer.visible? and @all_or == 2
										group.name = nil.to_s
									end
									if group.layer.visible? == false and @all_or == 3
										group.name = nil.to_s
									end
									if layer == group.layer and @all_or == 4
										group.name = nil.to_s
									end
									if sel.contains? group and @all_or == 5
										group.name = nil.to_s
									end
								end
							end
						end

						if @entitytype != "group"
							components.each do |component|
								if @all_or == 1
									if @what != "defname"
										component.name = nil.to_s
									end
									if @what != "name"
										component.definition.name = nil.to_s
									end
								end
								if component.layer.visible? and @all_or == 2
									if @what != "defname"
										component.name = nil.to_s
									end
									if @what != "name"
										component.definition.name = nil.to_s
									end
								end
								if component.layer.visible? == false and @all_or == 3
									if @what != "defname"
										component.name = nil.to_s
									end
									if @what != "name"
										component.definition.name = nil.to_s
									end
								end
								if layer == component.layer and @all_or == 4
									if @what != "defname"
										component.name = nil.to_s
									end
									if @what != "name"
										component.definition.name = nil.to_s
									end
								end
								if sel.contains? component and @all_or == 5
									if @what != "defname"
										component.name = nil.to_s
									end
									if @what != "name"
										component.definition.name = nil.to_s
									end
								end
							end
						end
					if @what == "defname" and @entitytype == "group"
						UI.messagebox "'Definition Name' and 'Groups' are not compatible, please select another option."
					end 

					empty = 0
					script = "top.empty = " + empty.to_s + ";"
					dialog.execute_script(script)

					Sketchup.send_action('selectSelectionTool:')

				end

				#Click Rename
				if params['rename'].to_s == "1"
					#########
					###RUN###
					#########
					model = Sketchup.active_model
					ents = model.active_entities
					groups = ents.grep(Sketchup::Group)
					components = ents.grep(Sketchup::ComponentInstance)
					layer = model.active_layer
					num = 0
					sel = model.selection

					############
					## Groups ##
					############
					if @entitytype != "comp"
						if @what != "defname"
							groups.each do |group|
								if @all_or == 1
									if group.name != "" and @keep == true
										group.name = "rnd_"+group.name
									else
										group.name = nil.to_s
									end
								end
								if group.layer.visible? and @all_or == 2
									if group.name != "" and @keep == true
										group.name = "rnd_"+group.name
									else
										group.name = nil.to_s
									end
								end
								if group.layer.visible? == false and @all_or == 3
									if group.name != "" and @keep == true
										group.name = "rnd_"+group.name
									else
										group.name = nil.to_s
									end
								end
								if layer == group.layer and @all_or == 4
									if group.name != "" and @keep == true
										group.name = "rnd_"+group.name
									else
										group.name = nil.to_s
									end
								end
								if sel.contains? group and @all_or == 5
									if group.name != "" and @keep == true
										group.name = "rnd_"+group.name
									else
										group.name = nil.to_s
									end
								end
							end

							groups.each do |group|
								if group.name == nil.to_s || group.name[0, 4] == "rnd_"
									num=0
								end

								if @all_or == 1
									groups.each do |g|
										
										if group.layer.name == g.layer.name
											if g.name == nil.to_s || g.name[0, 4] == "rnd_"
												
												if g.name[0, 4] == "rnd_"
													if @presu == 1
														keepname = " - #{g.name[4,100]}"
													else
														keepname = "#{g.name[4,100]} - "
													end
												else
													keepname = ""
												end
												
												num+=1
												bytenum = num.to_s.length
												if bytenum == 1
													zeros = "00"
												end
												if bytenum == 2
													zeros = "0"
												end
												if bytenum >= 3
													zeros = nil.to_s
												end
												if @addnum == 1
													if @presu == 1
															if @usename == 1
																g.name = "#{zeros}#{num} - "+(group.layer.name)+(keepname)
															else
																g.name = "#{zeros}#{num} - "+(@text_value)+(keepname)
															end
													else
														if @usename == 1
															g.name = (keepname)+group.layer.name+(" - #{zeros}#{num}")
														else
															g.name = (keepname)+@text_value+(" - #{zeros}#{num}")
														end
													end
												else
													if @usename == 1
														if @presu == 1
															g.name = group.layer.name+(keepname)
														else
															g.name = (keepname)+group.layer.name
														end
													else
														if @presu == 1
															g.name = @text_value+(keepname)
														else
															g.name = (keepname)+@text_value
														end
													end
												end
											end
										end
									end
								end
								if group.layer.visible? and @all_or == 2
									groups.each do |g|
										if group.layer.name == g.layer.name
											if g.name == nil.to_s || g.name[0, 4] == "rnd_"
												
												if g.name[0, 4] == "rnd_"
													if @presu == 1
														keepname = " - #{g.name[4,100]}"
													else
														keepname = "#{g.name[4,100]} - "
													end
												else
													keepname = ""
												end
											
												num+=1
												bytenum = num.to_s.length
												if bytenum == 1
													zeros = "00"
												end
												if bytenum == 2
													zeros = "0"
												end
												if bytenum >= 3
													zeros = nil.to_s
												end
												if @addnum == 1
													if @presu == 1
														if @usename == 1
															g.name = "#{zeros}#{num} - "+(group.layer.name)+(keepname)
														else
															g.name = "#{zeros}#{num} - "+(@text_value)+(keepname)
														end
													else
														if @usename == 1
															g.name = (keepname)+group.layer.name+(" - #{zeros}#{num}")
														else
															g.name = (keepname)+@text_value+(" - #{zeros}#{num}")
														end
													end
												else
													if @usename == 1
														if @presu == 1
															g.name = group.layer.name+(keepname)
														else
															g.name = (keepname)+group.layer.name
														end
													else
														if @presu == 1
															g.name = @text_value+(keepname)
														else
															g.name = (keepname)+@text_value
														end
													end
												end
											end
										end
									end
								end
								if group.layer.visible? == false and @all_or == 3
									groups.each do |g|
										if group.layer.name == g.layer.name
											if g.name == nil.to_s || g.name[0, 4] == "rnd_"
												
												if g.name[0, 4] == "rnd_"
													if @presu == 1
														keepname = " - #{g.name[4,100]}"
													else
														keepname = "#{g.name[4,100]} - "
													end
												else
													keepname = ""
												end
												
												num+=1
												bytenum = num.to_s.length
												if bytenum == 1
													zeros = "00"
												end
												if bytenum == 2
													zeros = "0"
												end
												if bytenum >= 3
													zeros = nil.to_s
												end
												if @addnum == 1
													if @presu == 1
															if @usename == 1
																g.name = "#{zeros}#{num} - "+(group.layer.name)+(keepname)
															else
																g.name = "#{zeros}#{num} - "+(@text_value)+(keepname)
															end
													else
														if @usename == 1
															g.name = (keepname)+group.layer.name+(" - #{zeros}#{num}")
														else
															g.name = (keepname)+@text_value+(" - #{zeros}#{num}")
														end
													end
												else
													if @usename == 1
														if @presu == 1
															g.name = group.layer.name+(keepname)
														else
															g.name = (keepname)+group.layer.name
														end
													else
														if @presu == 1
															g.name = @text_value+(keepname)
														else
															g.name = (keepname)+@text_value
														end
													end
												end
											end
										end
									end
								end
								if layer == group.layer and @all_or == 4
									groups.each do |g|
										if group.layer.name == g.layer.name
											if g.name == nil.to_s || g.name[0, 4] == "rnd_"
												
												if g.name[0, 4] == "rnd_"
													if @presu == 1
														keepname = " - #{g.name[4,100]}"
													else
														keepname = "#{g.name[4,100]} - "
													end
												else
													keepname = ""
												end
												
												num+=1
												bytenum = num.to_s.length
												if bytenum == 1
													zeros = "00"
												end
												if bytenum == 2
													zeros = "0"
												end
												if bytenum >= 3
													zeros = nil.to_s
												end
												if @addnum == 1
													if @presu == 1
															if @usename == 1
																g.name = "#{zeros}#{num} - "+(group.layer.name)+(keepname)
															else
																g.name = "#{zeros}#{num} - "+(@text_value)+(keepname)
															end
													else
														if @usename == 1
															g.name = (keepname)+group.layer.name+(" - #{zeros}#{num}")
														else
															g.name = (keepname)+@text_value+(" - #{zeros}#{num}")
														end
													end
												else
													if @usename == 1
														if @presu == 1
															g.name = group.layer.name+(keepname)
														else
															g.name = (keepname)+group.layer.name
														end
													else
														if @presu == 1
															g.name = @text_value+(keepname)
														else
															g.name = (keepname)+@text_value
														end
													end
												end
											end
										end
									end
								end
								if @all_or == 5
									groups.each do |g|
										if sel.contains? g
											if group.layer.name == g.layer.name
												if g.name == nil.to_s || g.name[0, 4] == "rnd_"
													
													if g.name[0, 4] == "rnd_"
														if @presu == 1
															keepname = " - #{g.name[4,100]}"
														else
															keepname = "#{g.name[4,100]} - "
														end
													else
														keepname = ""
													end
													
													num+=1
													bytenum = num.to_s.length
													if bytenum == 1
														zeros = "00"
													end
													if bytenum == 2
														zeros = "0"
													end
													if bytenum >= 3
														zeros = nil.to_s
													end
													if @addnum == 1
														if @presu == 1
																if @usename == 1
																	g.name = "#{zeros}#{num} - "+(group.layer.name)+(keepname)
																else
																	g.name = "#{zeros}#{num} - "+(@text_value)+(keepname)
																end
														else
															if @usename == 1
																g.name = (keepname)+group.layer.name+(" - #{zeros}#{num}")
															else
																g.name = (keepname)+@text_value+(" - #{zeros}#{num}")
															end
														end
													else
														if @usename == 1
															if @presu == 1
																g.name = group.layer.name+(keepname)
															else
																g.name = (keepname)+group.layer.name
															end
														else
															if @presu == 1
																g.name = @text_value+(keepname)
															else
																g.name = (keepname)+@text_value
															end
														end
													end
												end
											end
										end
									end	
								end

							end
						end
					end

					################
					## Components ##
					################
					if @entitytype != "group"
						components.each do |component|
							if @all_or == 1
								if @what != "defname"
									component.name = nil.to_s
								end
								if @what != "name"
									component.definition.name = nil.to_s
								end
							end
							if component.layer.visible? and @all_or == 2
								if @what != "defname"
									component.name = nil.to_s
								end
								if @what != "name"
									component.definition.name = nil.to_s
								end
							end
							if component.layer.visible? == false and @all_or == 3
								if @what != "defname"
									component.name = nil.to_s
								end
								if @what != "name"
									component.definition.name = nil.to_s
								end
							end
							if layer == component.layer and @all_or == 4
								if @what != "defname"
									component.name = nil.to_s
								end
								if @what != "name"
									component.definition.name = nil.to_s
								end
							end
							if sel.contains? component and @all_or == 5
								if @what != "defname"
									component.name = nil.to_s
								end
								if @what != "name"
									component.definition.name = nil.to_s
								end
							end
						end

						components.each do |component|
							if component.name == nil.to_s
								num=0
							end

							if @all_or == 1
								components.each do |c|
									if component.layer.name == c.layer.name
										if c.name == nil.to_s
											num+=1
											bytenum = num.to_s.length
											if bytenum == 1
												zeros = "00"
											end
											if bytenum == 2
												zeros = "0"
											end
											if bytenum >= 3
												zeros = nil.to_s
											end
											if @addnum == 1
												if @presu == 1
													if @usename == 1
														if @what != "defname"
															c.name = "#{zeros}#{num} - "+(component.layer.name)
														end
														if @what != "name"
															c.definition.name = "#{zeros}#{num} - "+(component.layer.name)
														end
													else
														if @what != "defname"
															c.name = "#{zeros}#{num} - "+(@text_value)
														end
														if @what != "name"
															c.definition.name = "#{zeros}#{num} - "+(@text_value)
														end
													end
												else
													if @usename == 1
														if @what != "defname"
															c.name = component.layer.name+(" - #{zeros}#{num}")	
														end
														if @what != "name"
															c.definition.name = component.layer.name+(" - #{zeros}#{num}")
														end
													else
														if @what != "defname"
															c.name = @text_value+(" - #{zeros}#{num}")
														end
														if @what != "name"
															c.definition.name = @text_value+(" - #{zeros}#{num}")
														end
													end
												end
											else
												if @usename == 1
													if @what != "defname"
														c.name = component.layer.name
													end
													if @what != "name"
														c.definition.name = component.layer.name
													end
												else
													if @what != "defname"
														c.name = @text_value
													end
													if @what != "name"
														c.definition.name = @text_value
													end
												end
											end
										end
									end
								end
							end
							if component.layer.visible? and @all_or == 2
								components.each do |c|
									if component.layer.name == c.layer.name
										if c.name == nil.to_s
											num+=1
											bytenum = num.to_s.length
											if bytenum == 1
												zeros = "00"
											end
											if bytenum == 2
												zeros = "0"
											end
											if bytenum >= 3
												zeros = nil.to_s
											end
											if @addnum == 1
												if @presu == 1
													if @usename == 1
														if @what != "defname"
															c.name = "#{zeros}#{num} - "+(component.layer.name)
														end
														if @what != "name"
															c.definition.name = "#{zeros}#{num} - "+(component.layer.name)
														end
													else
														if @what != "defname"
															c.name = "#{zeros}#{num} - "+(@text_value)
														end
														if @what != "name"
															c.definition.name = "#{zeros}#{num} - "+(@text_value)
														end
													end
												else
													if @usename == 1
														if @what != "defname"
															c.name = component.layer.name+(" - #{zeros}#{num}")	
														end
														if @what != "name"
															c.definition.name = component.layer.name+(" - #{zeros}#{num}")
														end
													else
														if @what != "defname"
															c.name = @text_value+(" - #{zeros}#{num}")
														end
														if @what != "name"
															c.definition.name = @text_value+(" - #{zeros}#{num}")
														end
													end
												end
											else
												if @usename == 1
													if @what != "defname"
														c.name = component.layer.name
													end
													if @what != "name"
														c.definition.name = component.layer.name
													end
												else
													if @what != "defname"
														c.name = @text_value
													end
													if @what != "name"
														c.definition.name = @text_value
													end
												end
											end
										end
									end
								end
							end
							if component.layer.visible? == false and @all_or == 3
								components.each do |c|
									if component.layer.name == c.layer.name
										if c.name == nil.to_s
											num+=1
											bytenum = num.to_s.length
											if bytenum == 1
												zeros = "00"
											end
											if bytenum == 2
												zeros = "0"
											end
											if bytenum >= 3
												zeros = nil.to_s
											end
											if @addnum == 1
												if @presu == 1
													if @usename == 1
														if @what != "defname"
															c.name = "#{zeros}#{num} - "+(component.layer.name)
														end
														if @what != "name"
															c.definition.name = "#{zeros}#{num} - "+(component.layer.name)
														end
													else
														if @what != "defname"
															c.name = "#{zeros}#{num} - "+(@text_value)
														end
														if @what != "name"
															c.definition.name = "#{zeros}#{num} - "+(@text_value)
														end
													end
												else
													if @usename == 1
														if @what != "defname"
															c.name = component.layer.name+(" - #{zeros}#{num}")	
														end
														if @what != "name"
															c.definition.name = component.layer.name+(" - #{zeros}#{num}")
														end
													else
														if @what != "defname"
															c.name = @text_value+(" - #{zeros}#{num}")
														end
														if @what != "name"
															c.definition.name = @text_value+(" - #{zeros}#{num}")
														end
													end
												end
											else
												if @usename == 1
													if @what != "defname"
														c.name = component.layer.name
													end
													if @what != "name"
														c.definition.name = component.layer.name
													end
												else
													if @what != "defname"
														c.name = @text_value
													end
													if @what != "name"
														c.definition.name = @text_value
													end
												end
											end
										end
									end
								end
							end
							if layer == component.layer and @all_or == 4
								components.each do |c|
									if component.layer.name == c.layer.name
										if c.name == nil.to_s
											num+=1
											bytenum = num.to_s.length
											if bytenum == 1
												zeros = "00"
											end
											if bytenum == 2
												zeros = "0"
											end
											if bytenum >= 3
												zeros = nil.to_s
											end
											if @addnum == 1
												if @presu == 1
													if @usename == 1
														if @what != "defname"
															c.name = "#{zeros}#{num} - "+(component.layer.name)
														end
														if @what != "name"
															c.definition.name = "#{zeros}#{num} - "+(component.layer.name)
														end
													else
														if @what != "defname"
															c.name = "#{zeros}#{num} - "+(@text_value)
														end
														if @what != "name"
															c.definition.name = "#{zeros}#{num} - "+(@text_value)
														end
													end
												else
													if @usename == 1
														if @what != "defname"
															c.name = component.layer.name+(" - #{zeros}#{num}")	
														end
														if @what != "name"
															c.definition.name = component.layer.name+(" - #{zeros}#{num}")
														end
													else
														if @what != "defname"
															c.name = @text_value+(" - #{zeros}#{num}")
														end
														if @what != "name"
															c.definition.name = @text_value+(" - #{zeros}#{num}")
														end
													end
												end
											else
												if @usename == 1
													if @what != "defname"
														c.name = component.layer.name
													end
													if @what != "name"
														c.definition.name = component.layer.name
													end
												else
													if @what != "defname"
														c.name = @text_value
													end
													if @what != "name"
														c.definition.name = @text_value
													end
												end
											end
										end
									end
								end
							end
							if @all_or == 5
								components.each do |c|
									if sel.contains? c
										if component.layer.name == c.layer.name
											if c.name == nil.to_s
												num+=1
												bytenum = num.to_s.length
												if bytenum == 1
													zeros = "00"
												end
												if bytenum == 2
													zeros = "0"
												end
												if bytenum >= 3
													zeros = nil.to_s
												end
												if @addnum == 1
													if @presu == 1
														if @usename == 1
															if @what != "defname"
																c.name = "#{zeros}#{num} - "+(component.layer.name)
															end
															if @what != "name"
																c.definition.name = "#{zeros}#{num} - "+(component.layer.name)
															end
														else
															if @what != "defname"
																c.name = "#{zeros}#{num} - "+(@text_value)
															end
															if @what != "name"
																c.definition.name = "#{zeros}#{num} - "+(@text_value)
															end
														end
													else
														if @usename == 1
															if @what != "defname"
																c.name = component.layer.name+(" - #{zeros}#{num}")	
															end
															if @what != "name"
																c.definition.name = component.layer.name+(" - #{zeros}#{num}")
															end
														else
															if @what != "defname"
																c.name = @text_value+(" - #{zeros}#{num}")
															end
															if @what != "name"
																c.definition.name = @text_value+(" - #{zeros}#{num}")
															end
														end
													end
												else
													if @usename == 1
														if @what != "defname"
															c.name = component.layer.name
														end
														if @what != "name"
															c.definition.name = component.layer.name
														end
													else
														if @what != "defname"
															c.name = @text_value
														end
														if @what != "name"
															c.definition.name = @text_value
														end
													end
												end
											end
										end
									end	
								end
							end

						end
					end
					if @what == "defname" and @entitytype == "group"
						UI.messagebox "'Definition Name' and 'Groups' are not compatible, please select another option."
					end 
					
					rename = 0
					script = "top.rename = " + rename.to_s + ";"
					dialog.execute_script(script)

					Sketchup.send_action('selectSelectionTool:')

				end

				dialog.set_on_close{ Sketchup.send_action('selectSelectionTool:') }

			end #def



			def unescape(string)
				if string != nil
					string = string.gsub(/\+/, ' ').gsub(/((?:%[0-9a-fA-F]{2})+)/n) do
						[$1.delete('%')].pack('H*')
					end
				end
				return string
			end #def



			def query_to_hash(query)
				param_pairs = query.split('&')
				param_hash = {}
				for param in param_pairs
					name, value = param.split('=')
					name = unescape(name)
					value = unescape(value)
					param_hash[name] = value
				end
				return param_hash
			end #def

		end #class

	end #module

end #module

file_loaded( __FILE__ )