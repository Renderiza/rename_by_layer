=begin
(c) Bagatelo 2013
(c) Renderiza 2013

Permission to use, copy, modify, and distribute this software for
any purpose and without fee is hereby granted, provided the above
copyright notice appear in all copies.

THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

== Information
Authors:: | Bagatelo | Renderiza |
Name:: Rename by Layer
Version:: 1.0.3
SU Version:: v8
Date:: 9/17/2013
Description:: Rename groups & components under same layer with layer name or name specified by user.
=end

module RND_Extensions

	module RND_Renamer
		require 'sketchup.rb'
		require 'RND_Renamer/RND_Renamer_loader.rb'
	end

	if !file_loaded?('rnd_menu_loader')
		@@rnd_tools_menu = UI.menu("Plugins").add_submenu("Renderiza Tools")
	end

	#------New menu Items---------------------------
	if !file_loaded?('rnd_ew_loader')
		@@rnd_ew_menu = @@rnd_tools_menu.add_submenu("Websites")
		@@rnd_ew_menu.add_separator
		@@rnd_ew_menu.add_item("PluginStore"){UI.openURL('http://sketchucation.com/resources/pluginstore?pauthor=renderiza')}
		@@rnd_ew_menu.add_item("Extension Warehouse"){UI.openURL('http://extensions.sketchup.com/en/user/5451/store')}
		@@rnd_tools_menu.add_separator
		@@rnd_ew_menu.add_separator
	end
	#------------------------------------------------

	if !file_loaded?(__FILE__) #then
		@@rnd_tools_menu.add_item('Rename by Layer') {
			Sketchup.active_model.select_tool RND_Extensions::RND_Renamer::Renamer.new
			Sketchup.send_action('selectSelectionTool:')
		}
		# Add toolbars
		rnd_renamer_tb = UI::Toolbar.new "Rename by Layer"
		rnd_renamer_cmd = UI::Command.new("Rename by Layer") {
			Sketchup.active_model.select_tool RND_Extensions::RND_Renamer::Renamer.new
			Sketchup.send_action('selectSelectionTool:')
		}

		# icons toolbar
		rnd_renamer_cmd.small_icon = "img/rnd_renamer_1_16.png"
		rnd_renamer_cmd.large_icon = "img/rnd_renamer_1_24.png"
		rnd_renamer_cmd.tooltip = "Rename by Layer"
		rnd_renamer_cmd.status_bar_text = "Rename groups & components under same layer with layer name or name specified by user."
		rnd_renamer_cmd.menu_text = "Rename by Layer"
		rnd_renamer_tb = rnd_renamer_tb.add_item rnd_renamer_cmd
		rnd_renamer_tb.show
	end

	file_loaded('rnd_ew_loader')
	file_loaded('rnd_menu_loader')
	file_loaded(__FILE__)
end